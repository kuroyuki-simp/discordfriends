from threading import Thread
import bottle
from bottle import get, run, request, response, redirect, template, static_file, post, HTTPError
import requests, os
import re
import asyncio
from urllib.parse import quote as urlencode
import mysql.connector
con = mysql.connector.connect(
  host="localhost",
  user="friends",
  password=os.environ["MYSQL_PASSWORD"],
  database="friends"
)

cur = None

# Discord stuff.
from discord.ext import commands
token = os.environ['TOKEN']
client = commands.Bot("df ")


USER = re.compile("[a-z0-9_]{1,15}")
NAME = re.compile("(.){4,18}")
AUTH = "https://discord.com/api/oauth2/authorize?client_id=851526056363032597&redirect_uri=https%3A%2F%2Ffriends.michaelsoftbinbows.uk%2Foauth&response_type=code&scope=identify%20guilds.join"
SERVER_INVITE = "https://discord.gg/7ryCCCKrwB"
API_ENDPOINT = 'https://discord.com/api/v8'
CLIENT_ID = '851526056363032597'
CLIENT_SECRET = os.environ["SECRET"]
REDIRECT_URI = 'https://friends.michaelsoftbinbows.uk/oauth'

def sql(rou):
  def wrapper(*args, **kwargs):
    global cur
    cur = con.cursor()
    ret = rou(*args, **kwargs)
    cur.close()
    return ret
  return wrapper

def check(rou):
  def wrapper(*args, **kwargs):
    if not request.get_cookie("discord_token", None):
      return redirect(AUTH)
    else:
      return rou(*args, **kwargs)
  return wrapper

def cookie(rou):
  def wrapper(*args, **kwargs):
    if not request.get_cookie("allow_cookies", False):
      return redirect("/cookies")
    else:
      return rou(*args, **kwargs)
  return wrapper 

def exchange_code(code):
  data = {
    'client_id': CLIENT_ID,
    'client_secret': CLIENT_SECRET,
    'grant_type': 'authorization_code',
    'code': code,
    'redirect_uri': REDIRECT_URI
  }
  headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
  r = requests.post('%s/oauth2/token' % API_ENDPOINT, data=data, headers=headers)
  r.raise_for_status()
  return r.json()


@get("/oauth")
@sql
@cookie
def oauth():
  auth = exchange_code(request.query["code"])["access_token"]
  response.set_cookie("discord_token", auth, httponly=True, max_age=1000)
  headers = {"Authorization": "Bearer " + auth, 'Content-Type': 'application/x-www-form-urlencoded'}
  USER_ID = requests.get("https://discord.com/api/v8/users/@me", headers=headers).json()["id"]
  headers2 = {"Authorization": "Bot " + token, 'Content-Type': 'application/json'}
  requests.put(f"https://discord.com/api/v8/guilds/851535774822170626/members/{USER_ID}", json={"access_token": auth}, headers=headers2)
  cur.execute("SELECT * FROM users WHERE id = %s", (USER_ID,))
  user = cur.fetchall()
  if len(user) == 0:
    print("Not found")
    return redirect("/finish")
  else:
    print("Found")
    return redirect("/dashboard")

@get("/finish")
@cookie
@check
def finish():
  return template("finish.html", error="")

@post("/finish")
@cookie
@sql
@check
def finish_post():
  auth = request.get_cookie("discord_token")
  headers = {"Authorization": "Bearer " + auth, 'Content-Type': 'application/x-www-form-urlencoded'}
  USER_ID = requests.get("https://discord.com/api/v8/users/@me", headers=headers).json()["id"]
  cur.execute("SELECT * FROM users WHERE handle = %s", (request.forms.get("handle").lower(),))
  handles = cur.fetchall()
  if len(handles) != 0:
    return template("finish.html", error="Handle is taken")
  cur.execute("SELECT * FROM users WHERE id = %s", (USER_ID,))
  user = cur.fetchall()
  if len(user) != 0:
    return redirect("/dashboard")
  if not re.fullmatch(USER, request.forms.get("handle").lower()):
    return template("finish.html", error="Invalid handle")
  if not re.fullmatch(NAME, request.forms.get("name")):
    return template("finish.html", error="Invalid name")
  
  user = (USER_ID, request.forms.get("name"), request.forms.get("handle").lower())
  cur.execute("insert into users (id, name, handle) values (%s, %s, %s)", user)
  return redirect("/dashboard")

@get("/info")
@cookie
@check
def info():
  auth = request.get_cookie("discord_token")
  headers = {"Authorization": "Bearer " + auth, 'Content-Type': 'application/x-www-form-urlencoded'}
  return requests.get("https://discord.com/api/v8/users/@me", headers=headers).content

@get("/")
@cookie
def index():
  return template("index.html", AUTH=AUTH)

@get("/particles.js")
def pjs():
  return template("particles.js")

@get("/particles.json")
def pjson1():
  return template("particles.json")


@get("/particles2.json")
def pjson():
  return template("particles2.json")


@get("/@<user>")
@sql
def profile(user):
  cur.execute("select * from users where handle = %s", (user, ))
  profile = cur.fetchall()
  if len(profile) == 0:
    return HTTPError(404, "User not found")
  profile = profile[0]
  
  headers = {"Authorization": "Bot " + token, 'Content-Type': 'application/x-www-form-urlencoded'}
  dname = requests.get("https://discord.com/api/v8/users/{0}".format(profile[0]), headers=headers).json()
  avatar = f"https://cdn.discordapp.com/avatars/{dname['id']}/{dname['avatar']}"
  dname = dname["username"] + "#" + dname["discriminator"]
  return template("profile.html", name=profile[1], handle=profile[2], pfp=avatar, username=dname)

@get("/cookies")
def consent():
  return template("consent.html")


@get("/consent")
def ack():
  response.set_cookie("allow_cookies", "sure")
  return redirect("/")


@get("/dashboard")
@cookie
@sql
@check
def dashboard():
  auth = request.get_cookie("discord_token")
  headers = {"Authorization": "Bearer " + auth, 'Content-Type': 'application/x-www-form-urlencoded'}
  USER_ID = requests.get("https://discord.com/api/v8/users/@me", headers=headers).json()["id"]
  cur.execute("select handle from users where id = %s", (USER_ID,))
  User = cur.fetchall()[0][0]

  return template("dashboard.html", error="", profile="/@" + User, server=SERVER_INVITE)

@post("/dashboard")
@check
@sql
@cookie
def changename():
  auth = request.get_cookie("discord_token")
  headers = {"Authorization": "Bearer " + auth, 'Content-Type': 'application/x-www-form-urlencoded'}
  USER_ID = requests.get("https://discord.com/api/v8/users/@me", headers=headers).json()["id"]
  cur.execute("select * from users where id = %s", (USER_ID,))
  User = cur.fetchall()[0]
  if not re.fullmatch(NAME, request.forms.get("name")):
    return template("dashboard.html", error="Invalid name", profile="/@" + User[2], server=SERVER_INVITE)
  cur.execute("update users set name = %s where id = %s", (request.forms.get("name"), USER_ID))
  return template("dashboard.html", error="Name changed successfully", profile="/@" + User[2], server=SERVER_INVITE)

@get("/logo")
def logo():
  return static_file("fd.png", ".")



import bot

client.add_cog(bot.Main(client, cur, "https://friends.michaelsoftbinbows.uk/@"))

loop = asyncio.get_event_loop()

def dis():
  asyncio.set_event_loop(loop)
  try:
      loop.run_until_complete(client.start(token))
  except KeyboardInterrupt:
      print("end")
      loop.run_until_complete(client.close())
      # cancel all tasks lingering
  finally:
      loop.close()
try:
  Thread(target=dis, daemon=True).start()
  run(host='127.0.0.1', port=8383, server='gunicorn', workers=4)
finally:
  con.commit()
  con.close()