from discord.ext import commands
from tinydb import TinyDB, Query

class Main(commands.Cog):
    def __init__(self, client, db, url):
        self.bot = client
        self.db = db
        self.url = url
    
    @commands.command(help="Get a users profile link given their id")
    async def profile(self, ctx: commands.Context, user_id: str):
        User = Query()
        User = self.db.search(User.id == user_id)
        if len(User) == 0:
            await ctx.reply("No profile found :(")
        else:
            await ctx.reply(self.url + User[0]["handle"])
